package model;


public class Floor implements CollisionObject {
    private int x;
    private int y;
    private int w; // height
    private int h; // width
    boolean floorIsMovingRight;


    public Floor(int x, int y, int w, int h, boolean floorIsMovingRight) {
        this.x = x;
        this.y = y;
        this.h = h;
        this.w = w;
        this.floorIsMovingRight = floorIsMovingRight;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }


    public void moveFloor(int dx) {
        this.x += dx;
    }
}
