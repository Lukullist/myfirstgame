package model;


import java.util.ArrayList;
import java.util.List;

public class Model {

    private Player player;
    private List<Level> levels = new ArrayList<>();
    private Level currentLvl;
    private Floor collisionFloor;
    private boolean collision;
    private byte finishCondition;  // 1 = loose, 2 = win
    private int graphicalDiscrepency;
    private int levelcounter;
    private int tolerance;


    public Model() {
        this.graphicalDiscrepency = -9200;
        tolerance = 70;
        levelcounter = 0;
        this.levels.add(new Level(levelcounter, tolerance));
        levelcounter++;
        this.currentLvl = levels.get(0);
        this.player = new Player(this.currentLvl.getFloors().get(0).getX(),
                this.currentLvl.getFloors().get(0).getY() - 50);

        finishCondition = 0;
    }

    public int getGraphicalDiscrepency() {
        return graphicalDiscrepency;
    }

    public Player getPlayer() {
        return player;
    }

    public int getLevelcounter() {
        return levelcounter;
    }

    public List<Floor> getFloors() {
        return currentLvl.getFloors();
    }

    public List<Level> getLevels() {
        return levels;
    }

    public Level getCurrentLvl() {
        return currentLvl;
    }

    public byte getFinishCondition() {
        return finishCondition;
    }

    public Floor getCollisionFloor() {
        return collisionFloor;
    }

    public boolean isCollision() {
        return collision;
    }

    public void update() {
        if (finishCondition == 0) {
            if (currentLvl.getLevelcount() >= levelcounter - 1) {
                tolerance *= 0.7;
                this.levels.add(new Level(levelcounter, tolerance));
                levelcounter++;
            }

            if (!player.resting) player.speedY += 1; // GravityStrength
            player.move(player.speedX, player.speedY);

            if (player.getY() > currentLvl.HEIGHT) {
                finishCondition = 1;
            }

            if (player.leftTension && !player.resting) player.move(-5, 0);
            if (player.rightTension && !player.resting) player.move(5, 0);

            if (checkCollisionFloors(player)) {
                collision = true;
                player.resting = true;
                player.speedY = 0;
                player.speedX = 0;
                player.y = collisionFloor.getY() - (player.getH() / 2 + collisionFloor.getH() / 2);
            }

            for (int i = 1; i < getFloors().size() - 1; i++) {
                Floor floor = getFloors().get(i);
                if ((!floor.floorIsMovingRight && floor.getX() < floor.getW() / 2) ||
                        (floor.floorIsMovingRight && floor.getX() + floor.getW() / 2 > currentLvl.WIDTH)) {
                    floor.floorIsMovingRight = !floor.floorIsMovingRight;

                }
                if (floor.floorIsMovingRight) {
                    floor.moveFloor((2 + levelcounter / 3));
                    if (player.resting && floor == collisionFloor) {
                        player.move((2 + levelcounter / 3), 0);
                    }
                } else {
                    floor.moveFloor(-(2 + levelcounter / 3));
                    if (player.resting && floor == collisionFloor) {
                        player.move(-(2 + levelcounter / 3), 0);
                    }
                }
            }

            if (player.getY() < 9700) {
                graphicalDiscrepency = -(player.getY() - 500);
            } else {
                graphicalDiscrepency = -((int) currentLvl.HEIGHT - 800);
            }
        }
    }


    public boolean checkCollisionFloors(Player player) {
        if (player.speedY > 0) {
            for (Floor floor : currentLvl.getFloors()) {
                if (checkCollision(player, floor)) {
                    collisionFloor = floor;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkCollision(Player player, CollisionObject object) {
        int dx = Math.abs(player.getX() - object.getX());
        int dy = Math.abs(player.getY() - object.getY());
        int w = player.getW() / 2 + object.getW() / 2;
        int h = player.getH() / 2 + object.getH() / 2;
        if (dx <= w && dy <= h) {
            return true;
        }
        return false;
    }

    public void activateGate(Gate gate) {

        if (checkCollision(player, gate)) {
            if (gate.getTarget() == -1) {
                finishCondition = 2;
                return;
            }
            currentLvl = levels.get(gate.getTarget());
            playerResett();
        }
    }

    private void playerResett() {
        player.x = this.currentLvl.getFloors().get(0).getX();
        player.y = this.currentLvl.getFloors().get(0).getY() - 50;
        player.resting = false;
    }

    public void restart() {
        this.graphicalDiscrepency = -9200;
        tolerance = 70;
        levelcounter = 0;
        this.levels.clear();
        this.levels.add(new Level(levelcounter, tolerance));
        levelcounter++;
        this.currentLvl = levels.get(0);
        playerResett();
        finishCondition = 0;

    }

    public void cheat() {
        player.x = currentLvl.getGate().getX();
        player.y = currentLvl.getGate().getY() - 10;
        player.resting = false;
    }
}
