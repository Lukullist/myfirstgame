package model;

public class Gate implements CollisionObject {
    private byte target;
    private int x;
    private int y;
    private int w; // height
    private int h; // width


    public Gate(Floor floor, byte target) {
        this.target = target;
        this.x = floor.getX();
        this.y = floor.getY() - floor.getH() / 2 - 15;
        this.h = 30;
        this.w = 30;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }

    public byte getTarget() {
        return target;
    }
}
