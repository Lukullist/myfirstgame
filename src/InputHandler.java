import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    private Model model;

    public InputHandler (Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode) {

        if (keycode == KeyCode.LEFT) {
            model.getPlayer().leftTension = true;
        }

        if (keycode == KeyCode.RIGHT) {
            model.getPlayer().rightTension = true;
        }

        if (keycode == KeyCode.SPACE) {
            model.getPlayer().jump();
            if (model.getFinishCondition() > 0) {
                model.restart();
            }
        }

        if (keycode == KeyCode.UP) {
            model.activateGate(model.getCurrentLvl().getGate());
        }

        if (keycode == KeyCode.PLUS) {
            model.cheat();
        }
    }

    public void onKeyReleased(KeyCode keyCode){

        if (keyCode == KeyCode.LEFT) {
            model.getPlayer().leftTension = false;
        }

        if (keyCode == KeyCode.RIGHT) {
            model.getPlayer().rightTension = false;
        }


    }
}
