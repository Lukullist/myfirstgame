package model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Level {

    private List<Floor> floors = new LinkedList<>();
    private Gate gate;
    private int levelcount;

    public final double WIDTH = 1000;
    public final double HEIGHT = 10000;

    public Level(int levelcount, int tolerance) {

        this.getFloors().addAll(createNewFloorLayOut(tolerance));
        this.levelcount = levelcount;

        this.gate = new Gate(this.getFloors().get(this.getFloors().size() - 1), (byte) (levelcount + 1));
    }

    public Gate getGate() {
        return gate;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public int getLevelcount() {
        return levelcount;
    }

    private List<Floor> createNewFloorLayOut(int tolerance) {
        List<Floor> floorLayout = new ArrayList<>();
        Random random = new Random();
        int x;
        int y;
        int w;
        int i = 0;
        int h = 20;
        int vZ = 1;

        floorLayout.add(new Floor(500, 9960, 200, 20,
                levelcount % 2 == 0));

        while (floorLayout.get(floorLayout.size() - 1).getY() > 200) {
            i++;
            w = (random.nextInt(100 + tolerance)) + 60;
            int rY = random.nextInt(51);
            y = (int) (floorLayout.get(floorLayout.size() - 1).getY() - (rY + 160) + tolerance * 0.5);
            x = 0;

            while (x < w / 2 || x > WIDTH - w / 2) {

                x = (int) (floorLayout.get(floorLayout.size() - 1).getX() + (vZ *
                        (100 + floorLayout.get(floorLayout.size() - 1).getW() / 2 + w / 2 - tolerance
                                + Math.sqrt(50 * (50 - rY)))));
                vZ *= -1;
            }
            vZ *= -1;
            floorLayout.add(new Floor(x, y, w, h, levelcount + i % 2 == 0));

        }

        return floorLayout;
    }


}
