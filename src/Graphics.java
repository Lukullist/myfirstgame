import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import model.Floor;
import model.Model;
import javafx.scene.paint.Color;


public class Graphics {

    private GraphicsContext gc;
    private Model model;

    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }

    public void draw() {
        gc.clearRect(0, 0, model.getCurrentLvl().WIDTH, 800);

        gc.setFill(Color.grayRgb(255 - ((int) (255 * Math.pow(0.8, model.getCurrentLvl().getLevelcount())))));
        gc.fillRect(0, 0, model.getCurrentLvl().WIDTH, 800);

        gc.setFill(Color.GREEN);
        if (model.getPlayer().isResting()) {
            gc.fillOval(model.getPlayer().getX() - model.getPlayer().getW()/2,
                    model.getGraphicalDiscrepency() + model.getPlayer().getY() - model.getPlayer().getH() / 2,
                    model.getPlayer().getW() + 10, model.getPlayer().getH());
        }
        else {
            gc.fillOval(model.getPlayer().getX() - model.getPlayer().getW()/2,
                    model.getGraphicalDiscrepency() + model.getPlayer().getY() - model.getPlayer().getH() / 2,
                    model.getPlayer().getW(), model.getPlayer().getH() + 10);
        }

        gc.setFill(Color.BLUE);
        for (Floor floor : model.getFloors()) {
            if (floor.getY() + model.getGraphicalDiscrepency() > 0) {
                gc.fillRect(floor.getX() - floor.getW() / 2,
                        model.getGraphicalDiscrepency() + floor.getY() - floor.getH() / 2,
                        floor.getW(), floor.getH());
            }
        }

        gc.setFill(Color.GOLD);
        gc.fillRect(model.getCurrentLvl().getGate().getX() - model.getCurrentLvl().getGate().getW() / 2,
                model.getGraphicalDiscrepency() + model.getCurrentLvl().getGate().getY() - model.getCurrentLvl().getGate().getH() / 2,
                model.getCurrentLvl().getGate().getW(),
                model.getCurrentLvl().getGate().getH());


        if (model.getFinishCondition() == 1) {
            /*gc.setFill(Color.BLACK);
            gc.fillRect(340, 65, 320, 170);*/
            gc.setFont(Font.font("Arial", 30));
            gc.setFill(Color.RED);
            gc.setFont(Font.font("Arial", FontWeight.BOLD, 25));
            gc.fillText("You lost.", 430, 100);
            gc.fillText("Press Space to restart.", 365, 200);
            gc.setStroke(Color.WHITE);
            //gc.setLineWidth(2);
            gc.strokeText("You lost.", 430, 100);
            gc.strokeText("Press Space to restart.", 365, 200);
        }

        if (model.getFinishCondition() == 2) {
            /*gc.setFill(Color.BLACK);
            gc.fillRect(340, 65, 320, 170);*/
            gc.setFont(Font.font("Arial", 30));
            gc.setFill(Color.GOLD);
            gc.setFont(Font.font("Arial", FontWeight.BOLD, 25));
            gc.fillText("You won!", 430, 100);
            gc.fillText("Press Space to restart.", 365, 200);
            gc.setStroke(Color.WHITE);
            //gc.setLineWidth(2);
            gc.strokeText("You won!", 430, 100);
            gc.strokeText("Press Space to restart.", 365, 200);
        }

    }

}
