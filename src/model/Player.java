package model;

public class Player implements CollisionObject {
    int x;
    int y;
    private int h; // height
    private int w; // width
    int speedX;
    int speedY;
    boolean resting;
    public boolean leftTension;
    public boolean rightTension;

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 25;
        this.w = 25;
        this.speedX = 0;
        this.speedY = 0;
        this.resting = false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public boolean isResting() {
        return resting;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public void jump() {
        if (!resting) return;
        this.speedY = -20;
        this.resting = false;
    }
}
