package model;

public interface CollisionObject {
    int getX();

    int getY();

    int getH();

    int getW();
}
